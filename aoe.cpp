#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>

using namespace std;

class Event{
public:
    int Ee = 0;
    int Le = 0;
    int InDegree = 0;
};

class Act{
public:
    int Index = 0;
    int Dur = 0;
    int StartNode = 0;
    int EndNode = 0;
    int E = 0;
    int L = 0;
    int Slack = 0;
    bool Critical = false;
};

vector <vector<int>> Adjacent;

void ConstructAdjMatrix(int NumberOfAct,int NumberOfEvent,vector <Act> ActList,vector <Event> &EventList){
    vector <int>Row(NumberOfEvent,-1);
    for(int i = 0;i < NumberOfEvent;++i){
        Adjacent.push_back(Row);
    }
    for(int i = 0;i < NumberOfAct;++i){
        Adjacent[ActList.at(i).StartNode][ActList.at(i).EndNode] = ActList.at(i).Dur;
        ++EventList.at(ActList.at(i).EndNode).InDegree;// Indegree counter for visit sequence
    }
}

void ConstructVisitingSeq(int NumberOfEvent,vector <Event> &EventList,vector <int> &VisitingSequence){
    for(int k = 0;k < NumberOfEvent;++k){
        for(int i = 0;i < NumberOfEvent;++i){
            if(EventList.at(i).InDegree == 0){
                VisitingSequence.push_back(i);
                //delete node
                EventList.at(i).InDegree = -1;
                //update indegree related with the deleted node
                for(int j = 0;j < NumberOfEvent; ++j){
                    if(Adjacent[i][j] != -1){
                        --EventList.at(j).InDegree;
                    }
                }
                break;
            }
        }
    }
}

void CaculateEe(int NumberOfEvent,vector <Event> &EventList,vector <int> VisitingSequence){
    vector<int> sorting;
    for(int i = 0;i < NumberOfEvent;++i){
        sorting.clear();
        int index = VisitingSequence.at(i);
        for(int check = 0;check < NumberOfEvent ;++check){
            if(Adjacent[check][index] != -1){
                sorting.push_back(Adjacent[check][index] + EventList[check].Ee);
            }
        }
        if(index > 0){// event 0 is starting point ,don't need to sort
            sort(sorting.begin(), sorting.end(), greater<int>());
            EventList[index].Ee = sorting.at(0);
        }
    }
}

void CaculateLe(int NumberOfEvent,vector <Event> &EventList,vector <int> VisitingSequence){
    vector<int> sorting;
    EventList[VisitingSequence.back()].Le = EventList[VisitingSequence.back()].Ee;//initialize last event
    for(int i = NumberOfEvent - 1;i >= 0;--i){
        sorting.clear();
        int index = VisitingSequence.at(i);
        for(int check = 0;check < NumberOfEvent ;++check){
            if(Adjacent[index][check] != -1){
                sorting.push_back(EventList[check].Le - Adjacent[index][check]);
            }
        }
        if(index  != VisitingSequence.back()){// last event is starting point ,don't need to sort
            sort(sorting.begin(), sorting.end());
            EventList[index].Le = sorting.at(0);
        }
    }
}

void CaculateAct(int NumberOfAct,vector <Act> &ActList,vector <Event> EventList){
    for(int i = 0;i < NumberOfAct;++i){
        ActList.at(i).E = EventList.at(ActList.at(i).StartNode).Ee;
        ActList.at(i).L = EventList.at(ActList.at(i).EndNode).Le - ActList.at(i).Dur;
        ActList.at(i).Slack = ActList.at(i).L - ActList.at(i).E;
        if(ActList.at(i).Slack == 0){
            ActList.at(i).Critical = true;
        }
    }
}

void PrintEvent(int NumberOfEvent,vector <Event> &EventList){
    cout << std::left << setw(8) << "event" << std::left << setw(8) << "ee" << std::left << setw(8) << "le" << endl;
    for(int i = 0;i < NumberOfEvent;++i){
        cout << std::left << setw(8) << i << std::left << setw(8) << EventList.at(i).Ee << std::left << setw(8) << EventList.at(i).Le << endl;
    }
}

void PrintAct(int NumberOfAct,vector <Act> ActList){
    cout << std::left << setw(8) << "act." << std::left << setw(8) << "e" << std::left << setw(8) << "l" << std::left << setw(8) << "slack" << std::left << setw(8) << "critical" << endl;
    for(int i = 0;i < NumberOfAct;++i){
        cout << std::left << setw(8) << ActList.at(i).Index << std::left << setw(8) << ActList.at(i).E << std::left << setw(8) << ActList.at(i).L << std::left << setw(8) << ActList.at(i).Slack;
        if(ActList.at(i).Critical){
            cout << "Yes" <<endl;
        }
        else{
            cout << "No" <<endl;
        }
    }
}

void PrintCritical(int NumberOfAct,vector <Act> ActList){
    for(int i = 0;i < NumberOfAct;++i){
        if(ActList.at(i).Critical){
            cout << ActList.at(i).Index << " ";
        }
    }
}

int main(){
    int NumberOfAct,NumberOfEvent = 0;
    vector <int> VisitingSequence;

    //input all data
    cin >> NumberOfAct;
    vector <Act> ActList(NumberOfAct);//be careful vector index start from 0
    for(int i = 0;i < NumberOfAct;++i){
        cin >> ActList.at(i).Index;
        cin >> ActList.at(i).StartNode;
        cin >> ActList.at(i).EndNode;
        cin >> ActList.at(i).Dur;
        if(ActList.at(i).EndNode > NumberOfEvent){//vertex counter
            NumberOfEvent = ActList.at(i).EndNode;
        }
    }
    ++NumberOfEvent;//vertex start from 0
    vector <Event> EventList(NumberOfEvent);


    ConstructAdjMatrix(NumberOfAct,NumberOfEvent,ActList,EventList);
    ConstructVisitingSeq(NumberOfEvent,EventList,VisitingSequence);
    CaculateEe(NumberOfEvent,EventList,VisitingSequence);
    CaculateLe(NumberOfEvent,EventList,VisitingSequence);
    CaculateAct(NumberOfAct,ActList,EventList);

    PrintEvent(NumberOfEvent,EventList);
    cout << endl << endl;
    PrintAct(NumberOfAct,ActList);
    cout << endl << endl;
    PrintCritical(NumberOfAct,ActList);
    cout << endl;
    return 0;
}

