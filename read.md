# AOE Network
In this program, I define two classes to store to type of data.
In class Event, it contain Ee which is event earliest time, Le which is event latest time and Indgree which is for deciding visiting sequence.
In class Act, it contain Index, Dur which is activity duration, StartNode which is activity starting vertex, EndNode which is activity ending vertex, E which is activity earliest time, L which is activity latest time , Slack ,and Critical which is for determinding whether the activity is critical activity or not.
Now start from the main function, first the user will input how many activities and info of each activities, I use a object vector to store all the data and record the maxium vertex index which is the number of vertexs.
Declare a object vector for managing event vertexs.
Call function ConstructAdjMatrix to construct adjacency matrix of the directed graph.
The row index of matrix is the begining vertex index of activties, while column index is the ending vertex index of activties and the data is the duration.
When constructing adjacency matrix, also record each vertex Indegree by checking the duration is not equal to -1;
Then call function ConstructVisitingSeq to construct Visiting Sequence which is also a vector and is passed by reference.
In function ConstructVisitingSeq, search the vertex whose indegree is 0, then delete it, also need to update the Indegree value of other vertexs who are related with the deleted vertex.
Now have all we need to caculate events' ee and le, call function CaculateEe and CaculateLe.
In function CaculateEe, visiting each vertex with the sequence recorded in sequence vector, vertex i's ee is decided by find the maxium value of other vertexs' ee + the duration of the activity that end with vertex i.
So  I use vector to recored all candidate and call built-in sort vector to find out the answer.
Function CaculateLe is almost the same, just start from the last vertex and use its ee to minus the duartion of activities backward.
After finding events' ee and e, activities' e and l are simple, follow the rule that:
Assume that activity ai is represented by edge <k, l>.
e(i) = ee[k]
l(i) = le[l] – duration of activity ai
which are all written on the textbook.
Slack is l(i) - e(i), and critical can be determinded by checking whether slack equal 0 or not.
Last thing to do is call functionPrintEvent, PrintAct, PrintCritical to print out put in a proper form.